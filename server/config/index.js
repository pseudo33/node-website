const path = require('path');

module.exports = {
  development: {
    sitename: 'NodePGH [Development]',
    data:{
      feedback: path.join(__dirname, '../data/feedback.json'),
    }
  },
  production: {
        sitename: 'Node PGH Meetup',
        data:{
          feedback: path.join(__dirname, '../data/feedback.json'),
        }
  },
}
