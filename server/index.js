const express = require('express');
const createError = require('http-errors');
const path = require('path');
const bodyParser = require('body-parser');
const configs = require('./config');
let health = require('@cloudnative/health-connect');
let healthcheck = new health.HealthChecker();
var prom = require('appmetrics-prometheus').attach();
var zipkin = require('appmetrics-zipkin')({
  host: 'localhost',
  port: 9411,
  serviceName:'nodepghserver',
  sampleRate: 1.0
});

const FeedbackService = require('./services/FeedbackService');
const app = express();

//ping check, to check outbound network connectivity
let pingcheck = new health.PingCheck("example.com");
healthcheck.registerReadinessCheck(pingcheck);

//healthcheck setup, takes healthchcek registers it to produce
// LivenessEndpoint and renders on /live url
app.use('/live', health.LivenessEndpoint(healthcheck));
app.use('/ready', health.ReadinessEndpoint(healthcheck));
//Central CONFIG
const config = configs[app.get('env')];
//const config = configs.production;

//instantiate feedback class
const feedbackService = new FeedbackService(config.data.feedback);

//VIEW
app.set('view engine', 'pug');
if (app.get('env') === 'development') {
  app.locals.pretty = true;
}
app.set('views', path.join(__dirname, './views'));
//set variable for title to use in .pug file
app.locals.title = config.sitename;


//ROUTES
const routes = require('./routes');
app.use(express.static('public'));

app.use(bodyParser.urlencoded({extended:true}));

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/favicon.ico', (req,res,next) =>{
  return res.sendStatus(204);
});

app.use('/', routes( {
  feedbackService,
}));

//Error Handler
app.use((req,res,next) => {
  return next(createError(404, 'File not Found X.X'));
});

app.use((err, req,res,next) => {
  res.locals.message = err.message;
  const status = err.status  || 500;
  res.locals.status = status;
  res.locals.error = req.app.get('env')=== 'development' ? err:{};
  res.status(status);
  return res.render('error');
});

//Where to Listen
app.listen(3000);

module.export = app;
