# [NodeJS PGH Meetup Site]

[![LICENSE](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://github.com/h5bp/html5-boilerplate/blob/master/LICENSE.txt)

A site created to learn node and express.

To Run:

`npm i`

`npm start`

Developmental Site: https://thinkcolorful.org:9999/
